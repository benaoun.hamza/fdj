# Drupal 8 Composer
Docker for FDJ: Nginx, PHP-FPM, MySQL and PHPMyAdmin.

## Overview

1. [Install prerequisites](#install-prerequisites)

    Before installing project make sure the following prerequisites have been met.

2. [Clone the project](#clone-the-project)

    We’ll download the code from its repository on gitlab.


3. [Run the application](#run-the-application)

    By this point we’ll have all the project pieces in place.

4. [add content and see the json on the browser](#add-content-and-see-the-json-on-the-browser)
   
    Add result and page node content.

4. [install npm & nodejs](#install-npm-nodejs) [for front react app]
 
   If you have nodejs and npm installed on your machine go to the next step.

5. [Start front application](#start-front-application)

    Run front app in your local.

___

## Install prerequisites

For now, this project has been mainly created for Unix `(Linux/MacOS)`. Perhaps it could work on Windows.

All requisites should be available for your distribution. The most important are :

* [Git](https://git-scm.com/downloads)
* [Docker](https://docs.docker.com/engine/installation/)
* [Docker Compose](https://docs.docker.com/compose/install/)

Check if `docker-compose` is already installed by entering the following command : 

```sh
which docker-compose
```

Check Docker Compose compatibility :

* [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/)

The following is optional but makes life better :

```sh
which make
```

### Images to use

* [Nginx](https://hub.docker.com/_/nginx/)
* [MySQL](https://hub.docker.com/_/mysql/)
* [PHP-FPM](https://hub.docker.com/r/woprrr/php-fpm/)
* [PHPMyAdmin](https://hub.docker.com/r/phpmyadmin/phpmyadmin/)

___

## Clone the project

To download and install following the instructions :

```sh
git clone git@gitlab.com:akramfathallah1/fdj.git
```

Go to the project directory :

```sh
cd fdj
```


## Run the application

1. Run docker :

   sudo docker-compose up -d

2. Install project dependencies :

    ```sh
   sudo make c-update

    ```

3. Install Drupal instance :

    ```sh
   sudo make drupal-si


4. Open your favorite browser :

    * [http://localhost:8000](http://localhost:8000/).
    * [http://localhost:8000/user](http://localhost:8000/user). (login : admin, pwd: admin)
    * [http://localhost:8091](http://localhost:8080/) PHPMyAdmin (username: root, password: root)



``
#add content
  - Add result content (http://localhost:8000/node/add/fdj_result)
  - Add page content (http://localhost:8000/node/add/fdj_page)
  - See the json of all fdj pages (Postman) : http://localhost:8000/apijson/list/fdj_page
  - See the json of all fdj pages with details of the results (Postman) : http://localhost:8000/apijson/list/fdj_page?includes=results
  - See the json of all fdj result (Postman) : http://localhost:8000/apijson/list/fdj_result
  - See the json of one fdj page (Postman) : http://localhost:8000/apijson/fdj_page/{nid}?includes=results
  - See the json of one fdj result (Postman) : http://localhost:8000/apijson/fdj_result/{nid}


 NB : To see the result on a small react application follow the next steps


## install npm & nodejs
   - sudo make install-nodejs 

## Start front application
   1. sudo make front 
   2. Open your favorite browser
     * [http://localhost:PORT](http://localhost:PORT/). --->  PORT : proposed port in terminal
   
