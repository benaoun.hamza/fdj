<?php

// Settings.
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
$settings['hash_salt'] = 'YUPiSjYMM6dI0dc1pkewk73fwcfPegWlNxbfVQD_mrZG2WmuDCDMa9h2_lYbBZfXRp7GLnK8ew';

// Settings NOT FOR PRODUCTION ENV.
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['extension_discovery_scan_tests'] = TRUE;
$settings['rebuild_access'] = TRUE;
$settings['file_chmod_directory'] = 0777;
$settings['file_chmod_file'] = 0777;

// Perf config for PROD.
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;
$config['system.performance']['cache']['page']['max_age'] = 900;

// Databases.
$databases['default']['default'] = [
  'database' => getenv('MYSQL_DATABASE'),
  'username' => getenv('MYSQL_ROOT_USER'),
  'password' => getenv('MYSQL_ROOT_PASSWORD'),
  'prefix' => '',
  'host' => getenv('MYSQL_HOST'),
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
];

// Config.
$config['system.logging']['error_level'] = 'verbose';

// Proxies.
$settings['http_client_config']['proxy']['http'] = getenv('PROXY_HTTP');
$settings['http_client_config']['proxy']['https'] = getenv('PROXY_HTTPS');
$settings['http_client_config']['proxy']['no'] = ['127.0.0.1', 'localhost'];

// Config directories.
$config_directories = [
  CONFIG_SYNC_DIRECTORY => getcwd() . '/../config',
];

//$settings['file_public_base_url'] = 'https://static.carrefour.com/sites/default/files';

// Trusted hostnames.
$c4com_hostnames = getenv('WEBSITE_HOSTNAME') . ',' . getenv('WEBSITE_HOSTNAME_VPOD');
$c4com_trusted_hostnames = [];
foreach (explode(',', $c4com_hostnames) as $c4com_hostname) {
  $c4com_trusted_hostnames[] = '^' . str_replace('.', '\.', $c4com_hostname) . '$';
}
$settings['trusted_host_patterns'] = $c4com_trusted_hostnames;

// Reverse proxy.
$settings['reverse_proxy'] = TRUE;
// TODO: for switch env.
//$settings['reverse_proxy_addresses'] = ['172.28.0.7'];
$remote_address = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
$settings['reverse_proxy_addresses'] = [$remote_address];
$settings['reverse_proxy_host_header'] = 'X_ORIGINAL_HOST';

// Limelight settings.
$settings['limelight_api'] = [
  'url' => 'https://api.video.limelight.com/rest/organizations/',
  'orgId' => '5e6db2f80ad24020a5c27cc502a50e6c',
  'accessKey' => 'B/nKJXNXbpCPN/MDGIQhJYQP+go=',
  'secret' => 'bWxFjIoMNZ69fWEfHo0abhCJqo0=',
  'use_proxy' => FALSE,
];



// Custom config: force web directory.
if (isset($GLOBALS['request']) and '/web/index.php' === $GLOBALS['request']->server->get('SCRIPT_NAME')) {
  $GLOBALS['request']->server->set('SCRIPT_NAME', '/index.php');
}
