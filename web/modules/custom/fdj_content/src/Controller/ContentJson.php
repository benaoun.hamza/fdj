<?php

namespace Drupal\fdj_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * GetEntity Controller.
 */
  class ContentJson extends ControllerBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityTypeManager;

    /**
     * Class constructor.
     *
     * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
     *   The entity manager.
     */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Static function (DI)
   */
  public static function create(ContainerInterface $container) {
    $entity_manager = $container->get('entity_type.manager');
    return new static($entity_manager);
  }

  /**
   * Entity json function (fdj_result & fdj_page).
   */
  public function getEntity($entity, $id) {
    $serializer = \Drupal::service('serializer');
    $node = $this->entityTypeManager->getStorage('node')->load($id);
    if($entity == $node->getType()){
    $data = $serializer->serialize($node, 'json');
    return new JsonResponse ([
      'data' => json_decode($data),
    ]);
    }
    return new JsonResponse ([
      'data' => NULL,
    ]);
  }

    /**
     * List json function (fdj_result & fdj_page).
     */
    public function getListContent($entity) {
      $data = [];
      $serializer = \Drupal::service('serializer');
      $query = $this->entityTypeManager->getStorage('node')->getQuery();
      $nids = $query->condition('type', $entity)
        ->condition('status', 1)
        ->sort('created', 'DESC')
        ->execute();
      $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
      foreach ($nodes as $key => $val) {
      array_push($data,  json_decode($serializer->serialize($val, 'json')));
      }
      return new JsonResponse ([
        'data' => $data,
      ] );
    }

}
