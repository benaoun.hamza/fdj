import React, {Component} from 'react';
import axios from 'axios'
import {Link} from "react-router-dom";
export default class EntityPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.wssource = "http://localhost:8000/";
  }

  //Function which is called when the component loads for the first time
  componentDidMount() {
    this.getPageDetails(this.props.match.params.id)
  }


  //Function to Load the pagedetails data from json.
    getPageDetails(id) {
    axios.get('http://localhost:8000/apijson/fdj_page/'+id+"?includes=results").then(response => {
      this.setState({result: response.data})
    })
  };

  render (){

      var title = "";
      var results = [];
      if(this.state.result){
          title = this.state.result.data.title[0].value;
          this.state.result.data.field_result_list.forEach(function(item){
              results.push(<li key={item.nid}><Link to={/resultat/+item.nid}  className='indent' key={item.nid}>{item.title}</Link></li>);
          });
      }
      return (<div className="result-list">
        <h1>Détails d'une page : </h1>
           <p>Titre : {title}</p>
           <p>Liste des resultats : </p>
          {results }
      </div>)
    }
}
